import sbt.Package.ManifestAttributes

name := "scalymer"

version := "2.0.3"

organization := "org.latestbit"

homepage := Some(url("http://www.latestbit.com"))

licenses := Seq(("Apache License v2.0", url("http://www.apache.org/licenses/LICENSE-2.0.html")))

scalaVersion := "2.12.6"

sbtVersion := "1.1.6"

publishMavenStyle := true

publishTo := {
	val nexus = "https://oss.sonatype.org/"
	if (isSnapshot.value)
		Some("snapshots" at nexus + "content/repositories/snapshots")
	else
		Some("releases" at nexus + "service/local/staging/deploy/maven2")
}

pomExtra := (
  <scm>
	  <url>https://bitbucket.org/latestbit/scalymer</url>
	  <connection>scm:hg:https://bitbucket.org/latestbit/scalymer</connection>
	  <developerConnection>scm:hg:https://bitbucket.org/latestbit/scalymer</developerConnection>
  </scm>
	<developers>
		<developer>
			<id>abdulla</id>
			<name>Abdulla Abdurakhmanov</name>
			<url>http://www.latestbit.com</url>
		</developer>
	</developers>
  )


resolvers ++= Seq(
	"Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
	"Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/",
	"Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"
)

enablePlugins(ScalaJSPlugin)

libraryDependencies ++= Seq(
	"org.scala-lang" % "scala-library" % scalaVersion.value,
	"org.scala-js" %%% "scalajs-dom" % "0.9.2",
	"com.typesafe.play" %%% "play-json" % "2.6.9",
	"org.scala-js" %%% "scalajs-java-time" % "0.2.4",
	"org.scalatest" %%% "scalatest" % "3.0.0" % "test"
)

scalacOptions ++= Seq(
	"-deprecation",
	"-unchecked",
	"-feature",
	"-Xsource:2.12",
	"-P:scalajs:sjsDefinedByDefault"
)

javacOptions ++= Seq("-Xlint:deprecation", "-source", "1.8", "-target", "1.8", "-Xlint")

packageOptions := Seq(ManifestAttributes(
	("Build-Jdk", System.getProperty("java.version")),
	("Build-Date", new java.text.SimpleDateFormat("MMddyyyy-hhmm").format(java.util.Calendar.getInstance().getTime))
))

jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv()

scalaJSLinkerConfig in (Test, fastOptJS) ~= { _.withESFeatures(_.withUseECMAScript2015(true)) }
