package org.latestbit.scalymer

/*
   Copyright 2018 Abdulla Abdurakhmanov (abdulla@latestbit.com)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


import org.latestbit.sjs.polymer2.Polymer
import org.latestbit.sjs.webanimations.{AnimationOptions, AnimationPlaybackEvent, Keyframes}
import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.Dynamic.global
import scala.scalajs.js.annotation.{JSExportStatic, JSExportTopLevel}

@JSExportTopLevel(name = "TestElement")
class TestElement() extends Polymer.Element {

	override def connectedCallback(): Unit = {
		super.connectedCallback()
		global.console.log(s"Attribute name ${getAttribute("name")}. Body is ${dom.document.querySelector("body")}")
		global.console.log(s"${this.$.selectDynamic("testCl")}")
		global.console.log(s"${$$("testCl")}")
		global.console.log(s"${$$("testCl2")}")
		testAnimate()
	}

	def testAnimate() = {
		$$("testCl").foreach { el =>

			el.animate(Keyframes.fromTuples(
				"opacity" -> 1,
				"opacity" -> 0.1,
				"opacity" -> 1
			).toJSArray, AnimationOptions(
				duration = 3000
			)).onfinish = { ev : AnimationPlaybackEvent =>
				global.console.log(s"Animation complete: ${ev}.")
			}
		}
	}

}

object TestElement {
	@JSExportStatic
	def is = "test-element"

	@JSExportStatic
	def properties = js.Dictionary(
		"name" -> js.Dictionary(
			"type" -> "String"
		)
	)
}