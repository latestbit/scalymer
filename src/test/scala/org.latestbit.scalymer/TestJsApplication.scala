package org.latestbit.scalymer

import org.latestbit.sjs.polymer2.Globals

import scala.scalajs.js

object TestJsApplication {

	def main() : Unit = {
		Globals.customElements.define(TestElement.is,
			js.constructorOf[TestElement]
		)
	}

}
