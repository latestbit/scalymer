package org.latestbit.sjs.polymer2

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExportAll

@JSExportAll
case class CustomEventOptions(bubbles :  Boolean = false, composed : Boolean = false, cancelable : Boolean = false) {
	def toJSDictionary : js.Dictionary[Any] = {
		js.Dictionary(
			"bubbles" -> bubbles,
			"composed" -> composed,
			"cancelable" -> cancelable
		)
	}
}
