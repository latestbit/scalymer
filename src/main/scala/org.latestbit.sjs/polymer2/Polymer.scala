package org.latestbit.sjs.polymer2

import org.latestbit.sjs.dom.HTMLTemplateElement
import org.latestbit.sjs.webanimations.AnimatedElement
import org.scalajs.dom
import org.scalajs.dom.NodeList
import org.scalajs.dom.ext._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

object Polymer {

	@js.native
	@JSGlobal("Polymer.Element")
	class NativeElement extends AnimatedElement {
		def connectedCallback() : Unit = js.native
		def disconnectedCallback() : Unit = js.native
		def adoptedCallback() : Unit = js.native

		def ready() : Unit = js.native
		def $ : js.Dynamic = js.native
		def shadowRoot : Element = js.native
		def notifyPath(path : String) : Unit = js.native

		def set(name : String, value : Any) : Unit = js.native
		def get(name : String) : Any = js.native
	}

	@js.native
	@JSGlobal("Polymer.IronMeta")
	class IronMeta(params : js.Dictionary[Any]) extends Polymer.NativeElement {

	}

	class ElementWrapper(nativeEl : NativeElement) {

		def ##(id : String) : Option[NativeElement] = {
			Option(nativeEl.$.selectDynamic(id)).filterNot(js.isUndefined).map(_.asInstanceOf[NativeElement])
		}

		def setElementProperty(name : String, value : Any) : ElementWrapper = {
			nativeEl.set(name,value)
			nativeEl.notifyPath(name)
			this
		}

		def getElementProperty[T <: Any](name : String) : Option[T] = {
			Option(nativeEl.get(name)).filterNot(js.isUndefined).map(_.asInstanceOf[T])
		}
	}

	object ElementWrapper {
		def wrap(nativeEl : NativeElement)  = new ElementWrapper(nativeEl)
	}

	class Element extends NativeElement {
		private var _shadowRoot : js.Dynamic = _
		private val wrapper = ElementWrapper.wrap(this)

		override def connectedCallback() : Unit = {
			super.connectedCallback()
			this._shadowRoot = shadowRoot.asInstanceOf[js.Dynamic]
		}

		def $$(selector : String) : List[NativeElement] = {
			val nodeList : NodeList = Option(_shadowRoot).map{ root =>
				root.applyDynamic(s"querySelectorAll")(selector).asInstanceOf[NodeList]
			}.getOrElse(dom.document.querySelectorAll(selector))

			nodeList.toList.map { node =>
				node.asInstanceOf[NativeElement]
			}
		}

		def ##(id : String) : Option[NativeElement] = {
			wrapper.##(id)
		}

		def getElementAttr[T <: Any](name : String) : Option[T] = {
			getElementAttr(this, name )

		}

		def getElementAttr[T <: Any](el : dom.Element, name : String) : Option[T] = {
			Option(el).flatMap(el => Option(el.getAttribute(name))).filterNot(js.isUndefined).map(_.asInstanceOf[T])
		}

		def setElementProperty(name : String, value : Any) : Element = {
			wrapper.setElementProperty(name,value)
			this
		}

		def getElementProperty[T <: Any](name : String) : Option[T] = {
			wrapper.getElementProperty(name)
		}
	}

	@js.native
	@JSGlobal(name = "Polymer")
	object Globals extends js.Any {
		def html(template : String) : HTMLTemplateElement = js.native
	}

}

