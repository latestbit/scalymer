package org.latestbit.sjs.polymer2

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobalScope

@js.native
@JSGlobalScope
object Globals extends js.Object {
	val customElements : CustomElements = js.native
}