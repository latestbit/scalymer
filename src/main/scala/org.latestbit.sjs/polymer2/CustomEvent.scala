package org.latestbit.sjs.polymer2

import org.scalajs.dom.raw.Event

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("CustomEvent")
class CustomEvent(val typeArg: String, body: Any) extends Event {
	def detail : js.Dictionary[Any] = js.native
}


object CustomEvent {
	def create[T <: Any](typeArg: String,
	                        detail: T,
	                        customEventOptions: CustomEventOptions = null): CustomEvent = {
		new CustomEvent(typeArg, js.Dictionary(
			"detail" -> detail,
			"easing" -> Option(customEventOptions).map(_.toJSDictionary).orNull
		))
	}
}