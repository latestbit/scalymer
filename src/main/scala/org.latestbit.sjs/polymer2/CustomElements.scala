package org.latestbit.sjs.polymer2

import scala.scalajs.js

@js.native
trait CustomElements extends js.Object {
	def define(name : String, constructor : js.Dynamic)
}
