package org.latestbit.sjs.dom

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("HTMLTemplateElement")
class HTMLTemplateElement extends dom.Element {

}
