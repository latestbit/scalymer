package org.latestbit.sjs.speech

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class SpeechRecognitionEvent extends dom.Event {
	val results : js.Array[js.Array[SpeechRecognitionAlternative]] = js.native
}


@js.native
trait SpeechRecognitionError extends dom.ErrorEvent {
	def error: String = js.native
}


@js.native
trait SpeechRecognitionAlternative extends js.Any {
	def transcript : String = js.native
	def confidence : Double = js.native
}
