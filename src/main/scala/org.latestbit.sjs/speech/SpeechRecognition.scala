package org.latestbit.sjs.speech

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("SpeechRecognition")
class SpeechRecognition extends js.Any {
	var lang : String = js.native

	def start() : Unit = js.native
	def stop() : Unit = js.native
	def abort(): Unit = js.native

	var onstart : js.Function1[dom.Event, Unit] = js.native
	var onend : js.Function1[dom.Event, Unit] = js.native

	var onresult : js.Function1[SpeechRecognitionEvent, Unit] = js.native
	var onspeechend : js.Function1[SpeechRecognitionEvent, Unit] = js.native

	var onerror: js.Function1[SpeechRecognitionError, Unit] = js.native
}
