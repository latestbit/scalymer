package org.latestbit.sjs.uaparser

import play.api.libs.json.{Json, Reads}

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportAll, JSGlobal}

@js.native
@JSGlobal("UAParser")
class UAParser() extends js.Any {
	def getResult() : js.Object = js.native
}

@JSExportAll
case class UserAgentInfo(ua : String,
                         browser: UABrowser = null,
                         device : UADevice  = null,
                         engine : UAEngine  = null,
                         os : UAOperationSystem = null)

@JSExportAll
case class UABrowser(name : String  = null, major : String  = null, version : String  = null)

@JSExportAll
case class UADevice(model : String = null, `type` : String  = null, vendor: String  = null)

@JSExportAll
case class UAEngine(name : String  = null, version : String  = null)

@JSExportAll
case class UAOperationSystem(name : String  = null, version : String  = null)

object UAParser {
	implicit val uaOperationSystemReads : Reads[UAOperationSystem] = Json.using[Json.WithDefaultValues].reads[UAOperationSystem]
	implicit val uaEngineReads : Reads[UAEngine] = Json.using[Json.WithDefaultValues].reads[UAEngine]
	implicit val uaDeviceReads : Reads[UADevice] = Json.using[Json.WithDefaultValues].reads[UADevice]
	implicit val uaBrowserReads : Reads[UABrowser] = Json.using[Json.WithDefaultValues].reads[UABrowser]
	implicit val uaInfoReads : Reads[UserAgentInfo] = Json.using[Json.WithDefaultValues].reads[UserAgentInfo]

	def parse() : Option[UserAgentInfo] = {
		val info = new UAParser().getResult()
		Json.fromJson[UserAgentInfo](Json.parse(js.JSON.stringify(info))).asOpt
	}
}