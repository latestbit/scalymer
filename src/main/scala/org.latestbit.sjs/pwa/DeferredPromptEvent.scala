package org.latestbit.sjs.pwa

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.Promise
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class DeferredPromptEvent extends dom.Event {
	def prompt() : Unit = js.native
	val userChoice : Promise[DeferredPromptChoiceResult] = js.native
}

@js.native
@JSGlobal
class DeferredPromptChoiceResult extends js.Any {
	val outcome : String = js.native
}