package org.latestbit.sjs.cookies

import org.scalajs.dom

import scala.scalajs.js

object CookiesManager {

	def readAll(): Map[String, Cookie] = {
		val all = Option(dom.document.cookie).map(_.split(';').flatMap { cookieVl =>
			cookieVl.split('=').toList match {
				case k :: v :: _ => {
					Some(k.trim() -> Cookie(
						name = k.trim(),
						value = js.URIUtils.decodeURIComponent(v)
					))
				}
				case _ => None
			}
		}.toMap).getOrElse(Map())
		all
	}

	private def cookieToString(cookie: Cookie): String = {
		s"${cookie.name}=${js.URIUtils.encodeURIComponent(cookie.value)};max-age=${cookie.maxAge};path=${cookie.path}"
	}

	def createCookie(cookie: Cookie) = {
		dom.document.cookie = cookieToString(cookie)
	}
}
