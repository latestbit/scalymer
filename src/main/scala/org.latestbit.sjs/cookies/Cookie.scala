package org.latestbit.sjs.cookies

case class Cookie(name: String,
                  value: String,
                  maxAge: Int = 31536000,
                  path: String = "/",
                  domain: String = null,
                  secure: Boolean = false,
                  httpOnly: Boolean = true)