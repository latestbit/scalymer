package org.latestbit.sjs.webanimations

import org.scalajs.dom.Event

import scala.concurrent.Future
import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class Animation extends js.Any {
	def finished : Future[Animation] = js.native
	var onfinish : js.Function1[AnimationPlaybackEvent, Unit] = js.native
}
