package org.latestbit.sjs.webanimations

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class AnimationPlaybackEvent extends dom.Event {
}
