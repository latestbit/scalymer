package org.latestbit.sjs.webanimations

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExportAll, JSGlobal}
import js.JSConverters._
import scala.language.implicitConversions

@js.native
@JSGlobal
class AnimatedElement extends org.scalajs.dom.Element {
	def animate(keyframes : js.Array[js.Dictionary[Any]], options : AnimationOptions) : Animation = js.native
}

@JSExportAll
case class AnimationOptions( delay : Double = 0,
                             easing : String = "linear",
                             endDelay : Double = 0,
                             fill : String = "none",
                             iterationStart : Double = 0,
                             iterations : Double = 1,
                             duration : Double = 0 )

case class Keyframes(items : Iterable[Keyframe]) {
	def toJSArray : js.Array[js.Dictionary[Any]] = {
		items.map { keyframe =>
			(keyframe.properties.toJSDictionary ++
			  Option(keyframe.options).map(_.toJSDictionary).getOrElse(js.Dictionary.empty)
			  ).toJSDictionary
		}.toJSArray
	}
}

object Keyframes {
	implicit def fromList(ms : (Map[String,Any])*): Keyframes = Keyframes(ms.map(Keyframe.of))
	implicit def fromTuples(ms : (String,Any)*): Keyframes = Keyframes(ms.map(Keyframe.of))
}

case class Keyframe(properties : Map[String,Any], options : KeyframeOptions = null )

object Keyframe {
	implicit def of(m : Map[String,Any]) : Keyframe = Keyframe(m)
	implicit def of(m : (String,Any)) : Keyframe = Keyframe(Map(m))
}

case class KeyframeOptions(offset : Double = 0, easing : String = null ) {
	def toJSDictionary : js.Dictionary[Any] = {
		Map(
			"offset" -> offset,
			"easing" -> easing
		).filter(_._2!=null).toJSDictionary
	}
}